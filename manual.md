# Manual

Find artists nearby who wish to collaborate by starting a new instant session. Or just look for a running session nearby.

1. Allow location detection or set a starting location by clicking on the map.

2. Within the blue markers popup, whether choose to just search, or to create an instant session. 

3. Alternatively, if you are logged in, you may choose to schedule a session.

To see details of any session, move you pointing device to the marker or click on it for even more details. 

Any session will be visible on the map for not longer than one hour. Start a new one if you are still in progress, so 
others know it is still time left to join. ;-)

To schedule a session, instead to start one instantly, you need an Microsoft account for authentication. You can cancel your scheduled sessions 
when logged in by selecting its marker on the map and clicking the cancel button within its popup. 

Hint: Logout, open a new browser window, login again for every new session you like to schedule. 

The screen will refresh every 5 minutes. Hit the refresh button of your browser to update in between this interval. 

** Please note: Although you may log into the app, you are still not authorized. You may get authorized upon request or invitation. **
