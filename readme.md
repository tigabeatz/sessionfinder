# About

I created this small web app to be used in the coming summer, 
to find other musicians for spontaneous sessions and to learn about azure web apps.


## Self hosting

Actually the app is prepared to run as Azure Web App (Python)

Configuration must be set in localconf.json in project root:

    {
      "db_account_name": "",
      "db_account_key": "",
      "web_contact_info": "",
      "web_legal_info": "",
      "map_url": "https://{s}.tile.openstreetmap.de/tiles/osmde/{z}/{x}/{y}.png",
      "map_attribution": "Kartendaten: OpenStreetMap, Mitwirkende, http://www.openstreetmap.org/copyright ",
      "ad_consumer_key": "",
      "ad_consumer_secret": "",
      "ad_tenant_name": "",
      "schedules_secret": ""
    }
    
or may be set via environment variables.

Java script dependencies need to be included manually:

- .min css and js files from https://github.com/nickpeihl/leaflet-sidebar-v2 
   need to be copied into static/sb
- copy the dist/images directory, awesome-markers.css, and awesome-markers.js 
  from https://github.com/lvoogdt/Leaflet.awesome-markers to static/am

Python dependencies are included in requirements.txt.

On Azure deploy by git repo, otherwise the dependencies will not get build.

these files / folders need to be added:
  .env
  localconf.json
  spare_code.py
  static/sidebar
  static/markers
  static/leaflet
  static/fontawesome

some manual changes with in the app services active directory may be needed.
remember the callback url for the MS oauth. as ssl happens outside of the app, 
the callback url will be http only. needs to be manually overwritten, for now. 

Users need to get managed within azure ad enterprise apps.

### Storage backends

Only azure tables actually

### Stored data

I try to collect as less data as possible within the app. 

- unique instant session or scheduled session id, 
- its position and type, the time of the request for one hour
- schedules also store an encrypted and hashed user identifier to allow for later editing through the user
- schedules are stored up to 7 days after creation
- logs and metrics mayy get collected, based upon the chosen provider

## Thanks to

- https://www.openstreetmap.de/
- https://stackoverflow.com/
- http://flask.pocoo.org/
- https://leafletjs.com/
- https://geopy.readthedocs.io/en/stable/
- https://python-markdown.github.io
- https://github.com/nickpeihl/leaflet-sidebar-v2
- https://github.com/lvoogdt/Leaflet.awesome-markers
- https://github.com/cicorias/python-flask-aad-v2
- https://github.com/microsoftgraph/python-sample-auth
- https://docs.microsoft.com/en-us/azure/active-directory/develop/howto-add-app-roles-in-azure-ad-apps
- https://dev.to/gimlet2/prometheus-python-flask-4ik0

