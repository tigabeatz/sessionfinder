
from geopy.distance import geodesic
import time
import os
import json
import markdown

from azure.cosmosdb.table.tableservice import TableService
from geopy.geocoders import Nominatim



# get real working dir?
try:
    with open('/home/site/wwwroot/readme.md') as f:
        mk = f.read()
    _cwd = '/home/site/wwwroot'
except FileNotFoundError:
    _cwd = os.path.join(os.path.abspath(__file__), os.pardir, os.pardir)


# load page content
def getreadme():
    # loads the readme markdown to be rendered in the sidebar later
    readme = os.path.join(_cwd, 'readme.md')
    with open(readme) as f:
        mk = f.read()
    rdme = markdown.markdown(mk)
    return rdme


def getmanual():
    # loads the manual markdown to be rendered in the sidebar later
    manual = os.path.join(_cwd, 'manual.md')
    with open(manual) as f:
        mk = f.read()
    mnl = markdown.markdown(mk)
    return mnl


def getform():
    # ugly, loading some javascript to inject into users map markers popup
    form = os.path.join(_cwd, 'static', 'form.js')
    with open(form) as f:
        html = f.read()
    return html


try:
    c_readme = getreadme()
    c_manual = getmanual()
    c_form = getform()
except:
    raise ValueError('Can not read page content files')


def getconfig(key):
    # configuration loader get credentials and metadata, also read from config file or env variables
    lclcnf = os.path.join(_cwd, 'localconf.json')
    localconfig = json.load(open(lclcnf, 'r', encoding='utf-8'))
    #print(type(localconfig), localconfig)
    config = {}
    config['name'] = 'Session Finder'
    config['version'] = 'alpha'
    config['datastore'] = {
        'type': 'aztables',
        # how long to keep the data
        'sessions_max_age_seconds': 3600,
        'schedules_max_age_seconds': 604800
    }
    config['types'] = ('yoga', 'paint', 'music', 'flow', 'code')
    config['code_link'] = 'https://gitlab.com/tigabeatz/sessionfinder'
    config['welcome_text'] = 'Session Finder | find, join or start sessions nearby'

    # expose some configs to environment variables. if not set use config file
    try:
        config['datastore']['access_config'] = {
            'account_name': os.getenv('account_name', default=localconfig['db_account_name']),
            'account_key': os.getenv('account_key', default=localconfig['db_account_key'])
        }

        config['contact_info'] = os.getenv('CONTACT_INFO', default=localconfig['web_contact_info'])
        config['legal_info'] = os.getenv('LEGAL_INFO', default=localconfig['web_legal_info'])
        config['de_map_url'] = os.getenv('DE_MAP_URL', default=localconfig['de_map_url'])
        config['de_map_attribution'] = os.getenv('DE_MAP_ATTRIBUTION', default=localconfig['de_map_attribution'])
        config['en_map_url'] = os.getenv('EN_MAP_URL', default=localconfig['en_map_url'])
        config['en_map_attribution'] = os.getenv('EN_MAP_ATTRIBUTION', default=localconfig['en_map_attribution'])
        config['rain_map_url'] = os.getenv('RAIN_MAP_URL', default=localconfig['rain_map_url'])
        config['rain_map_attribution'] = os.getenv('RAIN_MAP_ATTRIBUTION', default=localconfig['rain_map_attribution'])
        config['wind_map_url'] = os.getenv('WIND_MAP_URL', default=localconfig['wind_map_url'])
        config['wind_map_attribution'] = os.getenv('WIND_MAP_ATTRIBUTION', default=localconfig['wind_map_attribution'])
        config['temp_map_url'] = os.getenv('TEMP_MAP_URL', default=localconfig['temp_map_url'])
        config['temp_map_attribution'] = os.getenv('TEMP_MAP_ATTRIBUTION', default=localconfig['temp_map_attribution'])
        config['ad_consumer_key'] = os.getenv('AD_CONSUMER_KEY', default=localconfig['ad_consumer_key'])
        config['ad_consumer_secret'] = os.getenv('AD_CONSUMER_SECRET', default=localconfig['ad_consumer_secret'])
        config['ad_tenant_name'] = os.getenv('AD_TENANT_NAME', default=localconfig['ad_tenant_name'])
        config['schedules_secret'] = os.getenv('SCHEDULES_SECRET', default=localconfig['schedules_secret'])
    except:
        raise ValueError('error in getting environment variable or local config file')

    return config[key]



class DataStore:
    # default storage wrapper to be used with azure tables storage
    def __init__(self):
        self.config = getconfig('datastore')
        self.filtered_sessions = []

        self.table_service = TableService(getconfig('datastore')['access_config']['account_name'],
                                          getconfig('datastore')['access_config']['account_key'])
        # create azure table for instant and scheduled  sessions
        if not self.table_service.exists('sessions'):
            self.table_service.create_table('sessions')

        # Cleanup Instant Session Data older than time limit
        treshold1 = int(time.time()) - self.config['sessions_max_age_seconds']
        tasks = self.table_service.query_entities('sessions', filter="timestamp lt " + str(treshold1) +
                                                  " and schedule_when eq 'Now' ")
        for task in tasks:
            # delete
            self.table_service.delete_entity('sessions', task.PartitionKey, task.RowKey)

        # Cleanup Scheduled Session Data older than time limit
        treshold2 = int(time.time()) - self.config['schedules_max_age_seconds']
        tasks = self.table_service.query_entities('sessions', filter="timestamp lt " + str(treshold2) +
                                                  " and schedule_when ne 'Now' ")

        for task in tasks:
            # delete
            self.table_service.delete_entity('sessions', task.PartitionKey, task.RowKey)

    def append(self, data, targettable='sessions', targetpartition='sessions'):
        _isnew = True  # check if this artist has already started a session, then update position, or start new
        # insert or replace sessions
        data['PartitionKey'] = targetpartition
        data['RowKey'] = data['uuid']
        data['timestamp'] = int(data['timestamp'])
        self.table_service.insert_or_replace_entity(targettable, data)

    def delete(self, uuid, targettable='sessions', targetpartition='sessions'):
        self.table_service.delete_entity('sessions', 'sessions', str(uuid))


    def read(self, lat=8.2, lon=50.0, distance=10000, session_uuid='',
             dump=True, targettable='sessions', targetpartition='sessions'):

        # geolocation
        geolocator = Nominatim(user_agent="session_finder")



        if not dump:
            if session_uuid == '':
                # search all and limit by location, distance
                sessions = self.table_service.query_entities(targettable,
                                                             filter="PartitionKey eq '" + targetpartition + "'")
                for session in sessions:
                    dist = geodesic((lat, lon), (session.lat, session.lon)).km
                    if dist <= distance:
                        trow = {}
                        trow['distance'] = dist
                        for k in session.keys():
                            trow[k] = session[k]

                        # geoloc
                        ll = str(lat) + ',' + str(lon)
                        trow['address'] = str(geolocator.reverse(ll))

                        self.filtered_sessions.append(trow)
                return self.filtered_sessions
            else:
                # search by uuid
                sessions = self.table_service.query_entities(targettable,
                                                              filter="PartitionKey eq '" + targetpartition
                                                                     + "' and RowKey eq '" + session_uuid + "'")
                for session in sessions:
                        trow = {}
                        for k in session.keys():
                            trow[k] = session[k]

                        # geoloc
                        ll = str(trow['lat']) + ',' + str(trow['lon'])
                        trow['address'] = str(geolocator.reverse(ll))

                        self.filtered_sessions.append(trow)
                return self.filtered_sessions
        # data dump, get all stored data
        else:
            exp = []
            # search
            data = self.table_service.query_entities(targettable, filter="PartitionKey eq '" + targetpartition + "'")
            for schedule in data:
                trow = {}
                for k in schedule.keys():
                    trow[k] = schedule[k]

                # geoloc
                try:
                    ll = str(trow['lat']) + ',' + str(trow['lon'])
                    trow['address'] = str(geolocator.reverse(ll))
                    exp.append(trow)
                except:
                    trow['address'] = ''
            return exp


# some simple field validation
def validate(name, value):
    if name == 'lat' or name == 'lon':
        if not isinstance(value, (float,)):
            raise ValueError('no correct coordinates provided')
        else:
            output = value

    elif name == 'uuid':
        if not isinstance(value, (str,)):
            raise ValueError('no correct uuid provided')
        else:
            output = value

    elif name == 'timestamp':
        if not isinstance(value, (int,)):
            raise ValueError('no correct distance provided')
        else:
            output = value

    elif name == 'type':
        if value not in getconfig('types'):
            raise ValueError('invalid session or artist type provided')
        else:
            output = value

    elif name == 'create':
        if value not in ('session', 'schedule'):
            raise ValueError('invalid create type')
        else:
            output = value

    elif name == 'distance' or name == 'limit':
        if not isinstance(value, (int,)):
            raise ValueError('no correct distance provided')
        else:
            output = value
    else:
        output = value

    return output


# get the latest session
def latest():
    ds = DataStore()
    la = False
    for session in ds.read(dump=True):
        if not la:
            la = {
                'lat': session['lat'],
                'lon': session['lon'],
                'uuid': session['uuid'],
                'timestamp': session['timestamp'],
                'type': session['type'],
                'distance': session['distance']
            }
        else:
            if la['timestamp'] < session['timestamp']:
                la = {
                    'lat': session['lat'],
                    'lon': session['lon'],
                    'uuid': session['uuid'],
                    'timestamp': session['timestamp'],
                    'type': session['type'],
                    'distance': session['distance']
                }
    return json.dumps(la)
