#!/usr/bin/env python3
# ---------------------------------------------------------------------------------------------------------------------
#
# small web app to find nearby musicians to play with (or other artists to collaborate)
# each location will be stored for max 1h, loggend in users can announce sessions for up to 7 days upfront
#
# ---------------------------------------------------------------------------------------------------------------------
import uuid
import hashlib
from sfds.datastore import *
from flask import Flask, render_template, request, session, redirect, url_for
from flask_oauthlib.client import OAuth, OAuthException
from flask_session import Session

# web app
app = Flask(__name__)
app.secret_key = "mysecretkey"

# setting up the Azure AD login
oauth = OAuth(app)
tenant_name = getconfig('ad_tenant_name')  # Directory (tenant) ID
microsoft = oauth.remote_app(
    'microsoft',
    consumer_key=getconfig('ad_consumer_key'),  # Application (client) ID
    consumer_secret=getconfig('ad_consumer_secret'),  # from Keys/Password
    request_token_params={'scope': 'offline_access User.Read'},
    base_url='https://graph.microsoft.com/v1.0/',
    request_token_url=None,
    access_token_method='POST',
    access_token_url=str.format('https://login.microsoftonline.com/{tenant}/oauth2/v2.0/token', tenant=tenant_name),
    authorize_url=str.format('https://login.microsoftonline.com/{tenant}/oauth2/v2.0/authorize', tenant=tenant_name)
)


@app.route('/getadress', methods=['POST', 'GET'])
def geoloc():
    # setting address resolution
    try:
        geolocator = Nominatim(user_agent="session_finder")
        lat = validate('lan', request.args.get('latitude', type=float))
        lon = validate('lon', request.args.get('longitude', type=float))
        ll = str(lat) + ',' + str(lon)
        location = geolocator.reverse(ll)
        try:
            return json.dumps({'address': str(location)})
        except:
            return json.dumps({'address': ' '})
    except (ValueError, KeyError) as err:
        return json.dumps({'error': str(err)})


# new ui
@app.route('/', methods=['GET', ])
def ui_page():
    # generating the ui page, preload all data on first call

    try:
        type(session['uuid'])
    except KeyError:
        session['uuid'] = str(uuid.uuid4())
        session.modified = True

    try:
        type(session['ret_msg'])
    except KeyError:
        session['ret_msg'] = [str('reserved session id: ' + session['uuid']), ]
        session.modified = True

    # logged in?
    try:
        me = microsoft.get('me')
        session['ret_msg'].append('authorized user')
    except OAuthException:
        session['ret_msg'].append('anonymous user')
    ds = DataStore()
    try:
        sessions = session['data_sessions']
    except KeyError:
        sessions = ds.read(dump=True)

    try:
        username = session['username']
    except KeyError:
        username = ''

    return render_template("sf_app.html",
                           code_link=getconfig('code_link'),
                           legal_info=getconfig('legal_info'),
                           contact_info=getconfig('contact_info'),
                           readme=c_readme,
                           manual=c_manual,
                           welcome_text=getconfig('welcome_text'),
                           form_session=c_form,
                           de_map_url=getconfig('de_map_url'),
                           de_map_attribution=getconfig('de_map_attribution'),
                           en_map_url=getconfig('en_map_url'),
                           en_map_attribution=getconfig('en_map_attribution'),
                           temp_map_url=getconfig('temp_map_url'),
                           temp_map_attribution=getconfig('temp_map_attribution'),
                           wind_map_url=getconfig('wind_map_url'),
                           wind_map_attribution=getconfig('wind_map_attribution'),
                           rain_map_url=getconfig('rain_map_url'),
                           rain_map_attribution=getconfig('rain_map_attribution'),
                           sessions=sessions,
                           latest=latest(),
                           ret_msg=session['ret_msg'],
                           username=username
                           )


# sessions
@app.route('/sessions', methods=['GET', ])
def get_sessions():
    """ Get Sessions by LAT, LON, DISTANCE or by UUID or all """
    ds = DataStore()
    # https://127.0.0.1:5000/sessions?longitude=8.255958603920198&latitude=50.009674843728995&distance=10000
    try:
        session['latitude'] = validate('lan', request.args.get('latitude', type=float))
        session['longitude'] = validate('lon', request.args.get('longitude', type=float))
        session['distance'] = validate('distance', request.args.get('distance', type=int))
        ds = DataStore()
        session['data_sessions'] = ds.read(lat=session['latitude'], lon=session['longitude'],
                                           distance=session['distance'], dump=False)
        try:
            session['ret_msg'].append('got sessions for lat, lon, distance')
            session.modified = True
        except KeyError:
            session['ret_msg'] = []
            session['ret_msg'].append('got sessions for lat, lon, distance')
            session.modified = True
        return redirect(url_for('ui_page'))
    # https://127.0.0.1:5000/sessions?uuid=942826ad-a5f8-41d5-a5c9-30b06d7c8986
    except ValueError:
        try:
            session['uuid'] = validate('uuid', request.args.get('uuid', type=str))
            session['data_sessions'] = ds.read(dump=False, session_uuid=session['uuid'])
            session['ret_msg'].append('got sessions for uuid')
            session.modified = True
            return redirect(url_for('ui_page'))
        # https://127.0.0.1:5000/sessions
        except ValueError:
            try:
                session['ret_msg'].append('added session')
            except KeyError:
                session['ret_msg'] = []
                session['ret_msg'].append('added session')
            session['data_sessions'] = ds.read(dump=True)
            session['ret_msg'].append('got all session')
            session.modified = True
            return redirect(url_for('ui_page'))


@app.route('/sessions/delete', methods=['POST', 'GET'])
def delete_sessions():
    """  Delete by UUID """
    try:
        deluuid = validate('uuid', request.args.get('uuid', type=str))
        if not authorized:
            # session['managed_by']
            session['ret_msg'].append('can not cancel session. user is not authorized')
            session.modified = True
            return redirect(url_for('ui_page'))
        else:
            ds = DataStore()
            ds.delete(deluuid)
            session['ret_msg'].append('session cancelled')
            session.modified = True
    except:
        return render_template("sf_error.html", error=str('session already canceled or user not signed in'))
    return redirect(url_for('get_sessions'))


@app.route('/sessions/add', methods=['POST', 'GET'])
def post_sessions():
    """  Create or Update Schedules and Sessions. If no UUID provided, one is created """
    # setting the url parameters into the session cookie, creating UUID if not existent
    try:
        type(session['uuid'])
    except KeyError:
        session['uuid'] = str(uuid.uuid4())
        session.modified = True

    try:
        session['latitude'] = validate('lan', request.args.get('latitude', type=float))
        session['longitude'] = validate('lon', request.args.get('longitude', type=float))
        session['type'] = validate('type', request.args.get('type', type=str))
        session['distance'] = validate('distance', request.args.get('distance', type=int))
    except ValueError as err:
        return render_template("sf_error.html", error=str(err))

    session['create'] = validate('create', request.args.get('create', type=str))
    session['schedule_when'] = validate('schedule_when', request.args.get('schedule_when', type=str))
    # print(session)
    session.modified = True
    ds = DataStore()
    if session['create'] == 'schedule':
        if not authorized:
            session['ret_msg'].append('can not create session. user is not authorized')
            session.modified = True
            return redirect(url_for('ui_page'))
        else:
            schedule_secret = getconfig('schedules_secret')
            try:
                schedule_user = session['schedulemanager']
            except KeyError:
                try:
                    session['ret_msg'].append('can not create session. user is not authorized')
                except KeyError:
                    session['ret_msg'] = []
                    session['ret_msg'].append('can not create session. user is not authorized')
                return redirect(url_for('ui_page'))
            hashed_user = hashlib.sha512(schedule_user.encode(encoding='utf-8') +
                                         schedule_secret.encode(encoding='utf-8')).hexdigest()
            # create a scheduled session
            ds.append({
                'uuid': session['uuid'],
                'timestamp': str(int(time.time())),
                'schedule_when': session['schedule_when'],
                'managed_by': hashed_user,
                'lat': session['latitude'],
                'lon': session['longitude'],
                'type': session['type'],
                'distance': session['distance']
            })
            session['ret_msg'].append(str('created scheduled session: ' + str(session['uuid'])))
            session.modified = True
    else:
        # create instant session
        ds.append({
            'uuid': session['uuid'],
            'timestamp': str(int(time.time())),
            'schedule_when': 'Now',
            'managed_by': '',
            'lat': session['latitude'],
            'lon': session['longitude'],
            'type': session['type'],
            'distance': session['distance']
        })
        try:
            session['ret_msg'].append(str('created instant session: ' + str(session['uuid'])))
            session.modified = True
        except KeyError:
            session['ret_msg'] = []
            session['ret_msg'].append(str('created instant session: ' + str(session['uuid'])))
            session.modified = True

    return redirect(url_for('get_sessions', latitude=session['latitude'],
                            longitude=session['longitude'], distance=session['distance']))


@app.route('/login', methods=['POST', 'GET'])
def login():
    if 'microsoft_token' in session:
        session['ret_msg'].append('already logged in')
        return redirect(url_for('ui_page'))

    # Generate the guid to only accept initiated logins
    guid = uuid.uuid4()
    session['state'] = guid
    session['ret_msg'].append('logged in')
    # makes it to http so we need to set https manually
    # return microsoft.authorize(callback=url_for('authorized', _external=True), state=guid)
    return microsoft.authorize(callback='https://sessionfinder.azurewebsites.net/login/authorized', state=guid)
    # return microsoft.authorize(callback='https://127.0.0.1:5000/login/authorized', state=guid)


@app.route('/logout', methods=['POST', 'GET'])
def logout():
    session.pop('microsoft_token', None)
    session.pop('state', None)
    session.pop('username', None)
    session['ret_msg'].append('logged out')
    return redirect(url_for('ui_page'))


@app.route('/login/authorized')
def authorized():
    response = microsoft.authorized_response()
    if response is None:
        session['ret_msg'].append('error in authentication process')
        return "Access Denied: Reason=%s\nError=%s" % (
            response.get('error'),
            response.get('error_description')
        )

    # Check response for state
    if str(session['state']) != str(request.args['state']):
        raise Exception('State has been messed with, end authentication')

    # Okay to store this in a local variable, encrypt if it's going to client
    # machine or database. Treat as a password.
    session['microsoft_token'] = (response['access_token'], '')
    session['ret_msg'].append('aquired token')

    me = microsoft.get('me')
    session['username'] = me.data['displayName']
    session['schedulemanager'] = me.data['mail']

    session['managed_by'] = hashlib.sha512(session['schedulemanager'].encode(encoding='utf-8') +
                                           getconfig('schedules_secret').encode(encoding='utf-8')).hexdigest()

    session.modified = True

    return redirect(url_for('ui_page'))


# @app.route('/me')
# def me():
#     try:
#         me = microsoft.get('me')
#         session['username'] = me.data['displayName']
#         session.modified = True
#     except OAuthException:
#         session['ret_msg'].append('not authorized')
#         session.modified = True
#         return redirect(url_for('ui_page'))
#     return render_template('me.html', me=str(me.data))


@microsoft.tokengetter
def get_microsoft_oauth_token():
    return session.get('microsoft_token')


# turn of browser caching
@app.after_request
def add_header(r):
    """
    Add headers to both force latest IE rendering engine or Chrome Frame,
    and also to cache the rendered page for 10 minutes.
    """
    r.headers["Cache-Control"] = "no-cache, no-store, must-revalidate"
    r.headers["Pragma"] = "no-cache"
    r.headers["Expires"] = "0"
    r.headers['Cache-Control'] = 'public, max-age=0'
    return r


# for development purposes only
if __name__ == "__main__":
    app.config.update(SESSION_COOKIE_SECURE=True,
            REMEMBER_COOKIE_SECURE=True,
            SESSION_COOKIE_HTTPONLY=True,
            REMEMBER_COOKIE_HTTPONLY=True,
            SESSION_COOKIE_SAMESITE='Strong',)
    SESSION_TYPE = 'filesystem'
    app.config.from_object(__name__)
    Session(app)
    app.run(debug=False,
            ssl_context='adhoc',
            )
